#include "pch.h"
#include "MallardDuck.h"


MallardDuck::MallardDuck(int average_height, int average_weight, std::string desc) : 
	Duck(average_height, average_weight, desc, new NormalQuack("Em... Quack?!"), new FlyWithWings(9, 4, 5))
{
}


MallardDuck::~MallardDuck()
{
}
