#pragma once
#include "QuackBehaviour.h"

class Squeak :
	public QuackBehaviour
{
public:
	Squeak(std::string quack);
	~Squeak();
};

