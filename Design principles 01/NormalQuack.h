#pragma once
#include "QuackBehaviour.h"

class NormalQuack :
	public QuackBehaviour
{
public:
	NormalQuack(std::string);
	~NormalQuack() override;
};