#pragma once
#include "QuackBehaviour.h"
#include "FlyBehaviour.h"
#include <string>

class Duck
{
public:
	Duck(int, int, std::string, QuackBehaviour*, FlyBehaviour*);
	virtual ~Duck();

	void set_height(int h);
	void set_weight(int w);
	void set_description(std::string desc);
	void set_quack(std::string quack) {
		quack = { quack };
	}
	const std::string& get_description() const { return description; }
	
	virtual const std::string& perform_fly() const;

private:
	QuackBehaviour* quack{ nullptr };
	FlyBehaviour* fly{ nullptr };
	int average_height, average_weight;
	std::string description;
};