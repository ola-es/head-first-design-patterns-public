#pragma once
#include "Duck.h"
#include "NormalQuack.h"
#include "FlyWithWings.h"

class MallardDuck :
	public Duck
{
public:
	MallardDuck(int, int, std::string);
	~MallardDuck() override;
};