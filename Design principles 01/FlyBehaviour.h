#pragma once
#include <string>
class FlyBehaviour
{
	int m_per_sec, x_coordinate, y_coordinate;
	std::string message{ "fly behaviour performed" };
public:
	FlyBehaviour(int, int, int);

	const std::string& get_message() const;
	virtual const std::string& perform_behaviour() const;
	
	virtual ~FlyBehaviour();
};