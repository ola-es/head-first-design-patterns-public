#include "pch.h"
#include "NoFly.h"


NoFly::NoFly(int x, int y) : FlyBehaviour(0, x, y)
{
}

const std::string& NoFly::perform_behaviour() const 
{ 
	return FlyBehaviour::get_message(); 
}

NoFly::~NoFly()
{
}