#include "pch.h"
#include "QuackBehaviour.h"


QuackBehaviour::QuackBehaviour(std::string quack) : quack{ quack }
{
}

void QuackBehaviour::set_quack(std::string quack)
{ 
	quack = { quack }; 
}

const std::string& QuackBehaviour::perform_behaviour() const { return QuackBehaviour::quack; }

QuackBehaviour::~QuackBehaviour()
{
}