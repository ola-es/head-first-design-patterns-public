#pragma once
#include "FlyBehaviour.h"

class NoFly : public FlyBehaviour
{
public:
	NoFly(int, int);
	~NoFly() override;

	const std::string & perform_behaviour() const;
};