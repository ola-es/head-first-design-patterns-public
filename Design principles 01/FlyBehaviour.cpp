#include "pch.h"
#include "FlyBehaviour.h"


FlyBehaviour::FlyBehaviour(int m_per_sec, int x, int y) 
	: m_per_sec{ m_per_sec }, x_coordinate{ x }, y_coordinate { y }
{
}
const std::string& FlyBehaviour::get_message() const { return message; }

const std::string& FlyBehaviour::perform_behaviour() const
{
	return get_message();
}

FlyBehaviour::~FlyBehaviour()
{
}
