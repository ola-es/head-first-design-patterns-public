#pragma once
#include <string>
#include <iostream>

class QuackBehaviour
{
public:
	QuackBehaviour(std::string);

	void set_quack(std::string);
	virtual const std::string& perform_behaviour() const;

	virtual ~QuackBehaviour();
private:
	std::string quack;
};
