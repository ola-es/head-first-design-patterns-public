#include "pch.h"
#include "Duck.h"

Duck::Duck(int average_height, int average_weight, std::string desc, 
	QuackBehaviour* quack, FlyBehaviour* fly) : 
	average_height{ average_height }, 
	average_weight{ average_weight }, 
	description{ desc }, 
	quack{ quack }, 
	fly{ fly }
{
}

void Duck::set_height(int h) {
	Duck::average_height = { h };
}

void Duck::set_weight(int w) {
	Duck::average_weight = { w };
}

void Duck::set_description(std::string desc) {
	description = { desc }; 
}

const std::string& Duck::perform_fly() const {
	return fly->perform_behaviour();
}

Duck::~Duck()
{
}